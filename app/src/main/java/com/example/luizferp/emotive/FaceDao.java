package com.example.luizferp.emotive;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.Date;

/**
 * Created by luizp on 17/11/2017.
 */

@Dao
public interface FaceDao
{
    @Insert
    public void insertFaces(Face... faces);

    @Query("SELECT * FROM faces")
    public Face[] getFaces();

    @Query("SELECT * FROM faces WHERE creation_date > :date")
    public Face[] getFaces(Date date);

    @Query("DELETE FROM faces WHERE creation_date > :date")
    public void deleteFaces(Date date);
}
