package com.example.luizferp.emotive;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverter;
import android.support.annotation.NonNull;

import java.util.Date;
import java.util.UUID;


/**
 * Created by luizp on 17/11/2017.
 */

class Score
{
    public double anger;
    public double contempt;
    public double disgust;
    public double fear;
    public double happiness;
    public double neutral;
    public double sadness;
    public double surprise;
}

@Entity(tableName = "faces")
public class Face
{
    @NonNull
    @PrimaryKey
    public String faceId;

    @ColumnInfo(name = "creation_date")
    public Date creationDate;

    @ColumnInfo(name = "top")
    public Integer top;

    @ColumnInfo(name = "left")
    public Integer left;

    @ColumnInfo(name = "height")
    public Integer height;

    @ColumnInfo(name = "width")
    public Integer width;

    @Embedded
    public Score score;

    public Face(Integer top, Integer left, Integer height, Integer width, Score score)
    {
        this.faceId = UUID.randomUUID().toString();
        this.creationDate = new Date(System.currentTimeMillis());

        this.top = top;
        this.left = left;
        this.height = height;
        this.width = width;
        this.score = score;
    }

    static class DateTypeConverter
    {
        @TypeConverter
        public Date toDate(Long value)
        {
            return value == null ? null : new Date(value);
        }

        @TypeConverter
        public Long toLong(Date date)
        {
            return date == null ? null : date.getTime();
        }
    }
}
