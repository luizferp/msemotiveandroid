package com.example.luizferp.emotive;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;

/**
 * Created by luizp on 17/11/2017.
 */

@Database(entities = Face.class, version = 1)
@TypeConverters({Face.DateTypeConverter.class})
public abstract class FacesDatabase extends RoomDatabase
{
    private static volatile FacesDatabase INSTANCE;

    public abstract FaceDao faceDao();

    public static FacesDatabase getInstance(Context context)
    {
        if (INSTANCE == null)
        {
            synchronized (FacesDatabase.class)
            {
                if (INSTANCE == null)
                {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            FacesDatabase.class, "emotive.db")
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}
